// Object based preety Date for past & Future Dates

	var prettyDate = {

		checker : null,
	    years :  null,
	    months : null,
	    weeks : null,
	    days : null,
	    hours : null,
	    minutes :null,
	    seconds : null,
	    date1 : new Date(),
	    date2 : null
	};

  function DateDiff(date) {

    
    prettyDate.date2 = new Date(date);


    var difference = new DateMeasure(prettyDate.date1,prettyDate.date2);

    prettyDate.checker = difference.checker;

    prettyDate.years =  prettyDate.checker == "ago" ? difference.years : Math.abs(difference.years);
    prettyDate.months = prettyDate.checker == "ago" ? difference.months : Math.abs(difference.months);
    prettyDate.weeks = prettyDate.checker == "ago" ? difference.weeks : Math.abs(difference.weeks);
    prettyDate.days = prettyDate.checker == "ago" ? difference.days : Math.abs(difference.days);
    prettyDate.hours = prettyDate.checker == "ago" ? difference.hours : Math.abs(difference.hours);
    prettyDate.minutes = prettyDate.checker == "ago" ? difference.minutes : Math.abs(difference.minutes);
    prettyDate.seconds = prettyDate.checker == "ago" ? difference.seconds : Math.abs(difference.seconds);
    
    var result = new getResult(prettyDate);
    
  }


  function DateMeasure(date1,date2) {
    var d, h, m, s, w, mon, year,ms;

    ms = date1 - date2;
    s = Math.floor(ms / 1000);
    m = Math.floor(s / 60);
    s = s % 60;
    h = Math.floor(m / 60);
    
    m = m % 60;
    d = Math.floor(h / 24);
    h = h % 24;

    w = Math.floor(d / 7);
    mon = Math.floor(d / 30);
    year = Math.floor(d / 365);
    
    this.days = d;
    this.hours = h;
    this.minutes = m;
    this.seconds = s;
    this.weeks = w;
    this.months = mon;
    this.year = year;
    this.checker = date1 >  date2 ? "ago" : "to go";
  };

  function getResult(prettyDate){
  	if(prettyDate.years > 0){
    	document.getElementById("output").innerHTML = prettyDate.years+" years "+prettyDate.checker;
    }
    else if(prettyDate.months > 0){
    	document.getElementById("output").innerHTML = prettyDate.months+" months "+prettyDate.checker;
    }
    else if(prettyDate.weeks > 0){
    	document.getElementById("output").innerHTML = prettyDate.weeks+" weeks "+prettyDate.checker;
    }
    else if(prettyDate.days > 0){
    	document.getElementById("output").innerHTML = prettyDate.days+" days "+prettyDate.checker;
    }
    else if(prettyDate.hours > 0){
    	document.getElementById("output").innerHTML = prettyDate.hours+" hours "+prettyDate.checker;
    }
    else if(prettyDate.minutes > 0){
    	document.getElementById("output").innerHTML = prettyDate.minutes+" minutes "+prettyDate.checker;
    }
    else if(prettyDate.seconds > 0){
    	document.getElementById("output").innerHTML = prettyDate.seconds+" seconds "+prettyDate.checker;
    }

    return ;
  }




